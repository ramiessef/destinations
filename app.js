new Vue({
    el: '#app',
    data: {
        currentFilter: 'ALL',
        destinations: [{ //1
                "country": "Emirates Arabes Unis",
                "place": "Dubaï",
                "label": "Hyatt Regency Creek",
                "rating": "5",
                "upto": "Dès 67€",
                "redirect_label": "VERS LES EMIRATS ARABES UNIS — DUBAÏ",
                "tags": {
                    "classname": "option",
                    "label": "Surclassement offert"
                },
                image: "HYATT_REGENCY_CREEK",
                title: 'uae',
                slider: false
            },
            {
                "country": "Maurice",
                "place": "Grand Rivière",
                "label": "Laguna Beach Hotel & Spa",
                "rating": "4",
                "upto": "Jusqu'à -64%",
                "redirect_label": "VERS LES EMIRATS ARABES UNIS — DUBAÏ",
                "tags": {
                    "classname": "premium",
                    "label": "Tout inclus"
                },
                image: [{
                        image: "LAGUNA_BEACH",
                        active: 'active'
                    },
                    {
                        image: "LAGUNA_BEACH2",
                        active: ''
                    },
                    {
                        image: "LAGUNA_BEACH3",
                        active: ''
                    }
                ],
                title: 'Maurice',
                slider: true
            },
            { // 3
                "country": "Emirates Arabes Unis",
                "place": "Dubaï",
                "label": "Fairmont Dubaï",
                "rating": "5",
                "upto": "Dès 99€",
                "redirect_label": "VERS LES EMIRATS ARABES UNIS — DUBAÏ",
                "tags": {
                    "classname": "option",
                    "label": "Rooftop"
                },
                image: "FAIRMONT_DUBAI",
                title: 'uae2',
                slider: false
            }, { // 4
                "country": "Indonésie",
                "place": "Bali & Gili",
                "label": "Combiné Sthala - Marc - Patra",
                "rating": "0",
                "upto": "Dès 469€",
                "redirect_label": "VERS L'INDONÉSIE — BALI & GILI",
                "tags": {
                    "classname": "premium",
                    "label": "Combiné"
                },
                image: "STHALA_MARC_PATRA",
                title: 'Indonesie',
                slider: false
            },
            { // 5
                "country": "Maldives",
                "place": "Atoll de Noonu",
                "label": "Noku Maldives",
                "rating": "5",
                "upto": "Jusqu'à -36%",
                "redirect_label": "VERS MALDIVES — ATOLL DE NOONU",
                "tags": {
                    "classname": "option",
                    "label": "Massage offert"
                },
                image: "NOKU_MALDIVES",
                title: 'Maldives',
                slider: false
            },
            { // 6
                "country": "Thaïlande",
                "place": "Koh Samui",
                "label": "Impiana Resort Samui",
                "rating": "4",
                "upto": "Jusqu'à -70%",
                "redirect_label": "VERS LA THAÏLANDE — KOH SAMUI",
                "tags": {
                    "classname": "premium",
                    "label": "Massage offert"
                },
                image: [{
                        image: "IMPIANA_RESORT_SAMUI",
                        active: 'active'
                    },
                    {
                        image: "IMPIANA_RESORT_SAMUI2",
                        active: ''
                    },
                    {
                        image: "IMPIANA_RESORT_SAMUI3",
                        active: ''
                    }
                ],
                title: 'Thailande',
                slider: true
            },
            { // 7
                "country": "Maurice",
                "place": "Grand Baie",
                "label": "Clos du Littoral",
                "rating": "4",
                "upto": "Dès 538€",
                "redirect_label": "VERS MAURICE — GRAND BAIE",
                "tags": {
                    "classname": "option",
                    "label": "Villas avec Piscine Privée"
                },
                image: "CLOS_DU_LITTORAL",
                title: 'Maurice2',
                slider: false
            },
            { // 8
                "country": "Sri Lanka",
                "place": "Sri Lanka",
                "label": "Echappée SriLankaise",
                "rating": "3*",
                "upto": "Dès 699€",
                "redirect_label": "VERS LE SRI LANKA",
                "tags": {
                    "classname": "premium",
                    "label": "Circuit"
                },
                image: "ECHAPEE_SRI_LANKAISE",
                title: 'Sri-Lanka',
                slider: false
            },
            { // 9
                "country": "Japon",
                "place": "Tokyo",
                "label": "Grand Arc Hanzomon",
                "rating": "3",
                "upto": "Dès 114€",
                "redirect_label": "VERS LE JAPON — GRAND ARC HANZOMON",
                "tags": {
                    "classname": "premium",
                    "label": "City Break"
                },
                image: [{
                        image: "JAPON",
                        active: 'active'
                    },
                    {
                        image: "JAPON2",
                        active: ''
                    },
                    {
                        image: "JAPON3",
                        active: ''
                    }
                ],
                title: 'JAPON',
                slider: true
            },
            { // 10
                "country": "Vietnam",
                "place": "De Hanoï à Hoi An",
                "label": "Entre Culture et Plages",
                "rating": "4",
                "upto": "Dès 779€",
                "redirect_label": "VERS LE VIETNAM — DE HANOÏ À HOI AN",
                "tags": {
                    "classname": "premium",
                    "label": "Circuit"
                },
                image: "ENTRE_CULTURE_ET_PLAGES",
                title: 'Vietnam',
                slider: false
            }
        ]
    },
    methods: {
        setFilter: function (filter) {
            this.currentFilter = filter;
        },
        getWeatherData() {
            fetch("weather.json")
                .then(response => response.json())
                .then(data => (this.weatherDataList = data));
        }
    }
})

$(document).ready(function () {
    $(".filter-ctn").click(function () {
        $(".filters").css({
            height: "329px"
        });
    });
    $(".filters span.filter").click(function () {
        $(".filters span.filter:not(span.filter.active)").toggle("fail");
        $(".filter-ctn").toggle("fail");
        $(".filters").css({
            height: "auto"
        });
    });
});